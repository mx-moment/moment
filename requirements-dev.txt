remote_pdb >= 2.0.0,  < 3
pdbpp      >= 0.10.2, < 0.11
devtools   >= 0.12.0,  < 0.13

mypy                  >= 1.7.0,   < 1.11
flake8                >= 6.1.0,   < 8
flake8-isort          >= 6.1.0,   < 7
flake8-bugbear        >= 23.12.0, < 25
flake8-commas         >= 2.0.0,   < 3
flake8-comprehensions >= 3.3.0,   < 4
flake8-executable     >= 2.0.4,   < 3
flake8-logging-format >= 0.9.0,   < 1
flake8-pie            >= 0.16.0,  < 1
flake8-quotes         >= 3.2.0,   < 4
flake8-colors         >= 0.1.6,   < 0.2
