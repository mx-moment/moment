Pillow            >= 7.0.0,  < 11
aiofiles          >= 0.4.0,  < 25.0.0
appdirs           >= 1.4.4,  < 2
cairosvg          >= 2.4.2,  < 3
emoji             >= 2.0,    < 3.0
filetype          >= 1.0.7,  < 2
html_sanitizer    >= 1.9.1,  < 3
lxml              >= 4.5.1,  < 6
mistune           >= 2.0.0,  < 4.0
pymediainfo       >= 4.2.1,  < 7
plyer             >= 1.4.3,  < 3
sortedcontainers  >= 2.2.2,  < 3
watchfiles        >= 0.7
redbaron          >= 0.9.2,  < 1
hsluv             >= 5.0.0,  < 6
pyaudio           >= 0.2.14, < 1
dbus-python       >= 1.2.16, < 2; platform_system == "Linux"
matrix-nio[e2e]   >= 0.22.0, < 0.26

async_generator   >= 1.10,  < 2;   python_version < "3.7"
dataclasses       >= 0.6,   < 0.7; python_version < "3.7"
pyfastcopy        >= 1.0.3, < 2;   python_version < "3.8"
